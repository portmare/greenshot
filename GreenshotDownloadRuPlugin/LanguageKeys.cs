namespace GreenshotDownloadRuPlugin {
	public enum LangKey {
		upload_menu_item,
		settings_title,
		label_upload_format,
		upload_success,
		upload_failure,
		communication_wait,
		Configure,
		label_AfterUpload,
		label_AfterUploadLinkToClipBoard
	}
}
